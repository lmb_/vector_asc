﻿/*
 * Copyright (C) 2014-2021 Tobias Lorenz, 2021 Michal Borsuk.
 * Contact: tobias.lorenz@gmx.net; mborsuk@mborsuk.de
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Michal Borsuk.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once


#include <regex>

#include <Vector/ASC/platform.h>
#include <Vector/ASC/Event.h>
#include <Vector/ASC/CanSymbols.h>
#include <Vector/ASC/vector_asc_export.h>

namespace Vector {
namespace ASC {

/**
 * @brief Test-related info
 */
struct VECTOR_ASC_EXPORT TestStructure final : Event {
    TestStructure() : Event(EventType::TestStructure) {}

    /** @copydoc Event::read() */
    static TestStructure * read(File & file, std::string & line);

    virtual void write(File & file, std::ostream & stream) override;

    /** @copydoc Time */
    Time time {0.0};

    /**
     * @brief unique ID identifying the executing test module or test configuration
     */
    uint32_t executionObjectIdentify {};

    /** enumeration for type */
    enum Type : uint16_t {
        TM_TESTMODULE = 1,
        TM_TESTGROUP = 2,
        TM_TESTCASE = 3,
        TESTCONFIGURATION = 8,
        TESTUNIT = 9,
        TESTGROUP = 10,
        TESTFIXTURE = 11,
        TESTSEQUENCE = 12,
        TESTSEQUENCELIST = 13,
        TESTCASE = 14,
        TESTCASELIST = 15
    };

    /**
     * @brief type of structure element
     */
    uint16_t type {};

    /**
     * @brief unique number of structure element (in this test run, transitive,
     *  can be used to correlate begin/end events)
     */
    uint32_t uniqueNo {};

    /** enumeration for action */
    enum Action : uint16_t {
        UNDEFINED = 0,
        BEGIN = 1,
        END = 2,

        /**
         * early abortion of test execution (due to e.g. verdict impact,
         *  user stop or failed assure pattern),  always correlated to test
         *  module / test configuration and followed by "end" action
         */
        ABORT = 3
    };

    /**
     * @brief indicates begin/end of structure element
     */
    uint16_t action {};

    /** enumeration for result */
    enum Result : uint16_t {
        UNDEF = 0,
        NONE = 1,
        PASSED = 2,
        INCONCLUSIVE = 3,
        FAILED = 4,
        ERRORINTESTSYSTEM = 5
    };

    /**
     * @brief overall result (verdict) for end of structure element events
     */
    uint16_t result {};

    /**
     * @brief name of the executing test module or test configuration as shown in CANoe (wchar_t)
     */
    std::string executingObjectName {};

    /**
     * @brief name of structure element (can change between begin/end when using CAPL function TestCaseTitle or similar (wchar_t)
     */
    std::string name {};


    std::string testModule, testGroup, testUnit, testCase;
};

}
}
