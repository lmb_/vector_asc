/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iomanip>
#include <regex>
#include <sstream>

#include <Vector/ASC/CanCommon.h>
#include <Vector/ASC/CanMessage.h>
#include <Vector/ASC/CanSymbolsRegEx.h>

namespace Vector {
namespace ASC {

CanMessage * CanMessage::read(File & file, std::string & line) {
    static const std::regex regex(REGEX_STOL REGEX_Timestamp
                REGEX_WS REGEX_Channel
                REGEX_WS "([[:xdigit:]]{1,14}x?|[[:alnum:]_]+)"
                REGEX_WS REGEX_Dir
                REGEX_WS "d"
                REGEX_WS REGEX_DLC
                "((" REGEX_WS REGEX_Dx "){0,8})"
                "(" REGEX_WS "Length" REGEX_ws "=" REGEX_ws REGEX_MessageDuration ")?"
                "(" REGEX_WS "BitCount" REGEX_ws "=" REGEX_ws REGEX_MessageLength ")?"
                "(" REGEX_WS REGEX_MessageFlags ")?"
                "(" REGEX_WS "ID" REGEX_ws "=" REGEX_ws REGEX_IDnum ")?"
            REGEX_ENDL);
    std::smatch match;
    if (std::regex_match(line, match, regex)) {
        auto * canMessage = new CanMessage;
        canMessage->time = stodC(match[1]);
        canMessage->channel = std::stoul(match[2]);
        try {
            canMessage->id = std::stoul(match[3], nullptr, file.base);
            if (std::string(match[3]).back() == 'x')
                canMessage->id |= 0x80000000;
        }  catch (std::invalid_argument &ex) {
            // sometimes try returns false-positives for messageName,
            //  ignore them  - processing at match[14] below
        }
        if (match[4] == "Rx")
            canMessage->dir = Dir::Rx;
        else if (match[4] == "Tx")
            canMessage->dir = Dir::Tx;
        canMessage->dlc = std::stoul(match[5], nullptr, file.base);
        std::istringstream iss(match[6]);
        if (file.base == 10)
            iss >> std::dec;
        else if (file.base == 16)
            iss >> std::hex;
        while (!iss.eof() && (canMessage->data.size() < canMessage->dlc)) {
            unsigned short s;
            iss >> s;
            canMessage->data.push_back(s);
        }
        if (match[8] != "")
            canMessage->messageDuration = std::stoul(match[9]);
        if (match[10] != "")
            canMessage->messageLength = std::stoul(match[11]);
        if (match[12] != "") {
            if (match[13] == "TE")
                canMessage->messageFlags.te = true;
            else if (match[13] == "WU")
                canMessage->messageFlags.wu = true;
            else if (match[13] == "XX") {
                canMessage->messageFlags.te = true;
                canMessage->messageFlags.wu = true;
            }
        }

        if (match[14] != "") {
            if(canMessage->id == 0){
                canMessage->id = std::stoul(match[15]); // always decimal!
                // This is a guesstimate..
                if ((canMessage->id & 0x10000000) == 0x10000000)
                    canMessage->id |= 0x80000000;
            }
            canMessage->messageName = match[3];
        }


        return canMessage;
    }else{
        std::cerr << "Regex match fail in '" << __PRETTY_FUNCTION__
                  << "' for line " << std::endl
                  << "\t" << line << std::endl;
    }
    return nullptr;
}

void CanMessage::write(File & file, std::ostream & stream) {
    writeTime(file, stream, time);
    stream << ' ' << std::dec << (uint16_t) channel << "  ";
    switch (file.base) {
    case 10:
        stream << std::left << std::setfill(' ') << std::setw(15) << std::dec << id;
        break;
    case 16:
        stream << std::left << std::setfill(' ') << std::setw(15) << std::hex << id;
        break;
    }
    stream << ' ';
    writeDir(file, stream, dir);
    stream
            << " d " << std::hex << (uint16_t) dlc
            << std::uppercase;
    writeData(file, stream, data);

    if (file.version >= File::Version::Ver_7_5) {
        stream << ' ';

        /* format: " Length= " */
        stream << " Length = ";

        stream << std::dec << messageDuration;

        /* format: " BitCount = " */
        stream << " BitCount = ";

        stream << std::dec << messageLength;
    }

#if 0
    /* <MessageFlags> */
    if (!messageFlags.empty())
        stream << ' ' << std::dec << messageFlags;
#endif

    if (file.version >= File::Version::Ver_8_0) {
        /* format: " ID = " */
        stream << " ID = ";

    }

    stream << endl;
}

}
}
