/*
 * Copyright (C) 2013-2021 Tobias Lorenz, 2021 Michal Borsuk.
 * Contact: tobias.lorenz@gmx.net; mborsuk@mborsuk.de
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Michal Borsuk.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <regex>

#include <Vector/ASC/TestStructure.h>
#include <Vector/ASC/CanSymbolsRegEx.h>

namespace Vector {
namespace ASC {

#define REGEX_32b_hex "([[:xdigit:]]{8})"
#define REGEX_uniqueNo REGEX_32b_hex
#define REGEX_QoutedTxt "'([^']*)'"
#define REGEX_ComCol "(,|:)?"

TestStructure *TestStructure::read(File &/*file*/, std::string &line)
{
    static const std::regex regex(REGEX_STOL REGEX_Timestamp
                REGEX_WS "TFS:"
                REGEX_WS "\\[" REGEX_32b_hex "," REGEX_uniqueNo "\\]"
                "(" REGEX_WS "(Passed|Failed):)?"
                "(" REGEX_WS "(Test configuration)" REGEX_WS REGEX_QoutedTxt REGEX_ComCol")?"
                "(" REGEX_WS "(Test module)" REGEX_WS REGEX_QoutedTxt REGEX_ComCol")?"
                "(" REGEX_WS "(Test group)" REGEX_WS REGEX_QoutedTxt REGEX_ComCol ")?"
                "(" REGEX_WS "(Test unit)" REGEX_WS REGEX_QoutedTxt REGEX_ComCol")?"
                "(" REGEX_WS "(Test case)" REGEX_WS REGEX_QoutedTxt REGEX_ComCol")?"
                REGEX_WS "(started|finished)\\."
            REGEX_ENDL);
    std::smatch match;
    if (std::regex_match(line, match, regex)) {

        auto * testStructure = new TestStructure;

        // Field 'executionObjectIdentify' cannot be extracted
        //  here, try "upstream" from executionObjectName

        testStructure->time = stodC(match[1]);
        // field match[2] unknown
        testStructure->uniqueNo = std::stoul(match[3], nullptr, 16);

        // Result
        if(match[5] == "Passed")
            testStructure->result = Result::PASSED;
        else if (match[5] == "Failed")
            testStructure->result = Result::FAILED;
        else
            testStructure->result = Result::UNDEF;

        // NOTE: order of entries is important!
        // Test configuration
        if(match[7] == "Test configuration"){
            testStructure->executingObjectName = match[8];
            testStructure->type = Type::TESTCONFIGURATION;
        }

        // Test module
        if(match[11] == "Test module"){
            testStructure->testModule = match[12];
            testStructure->type = Type::TM_TESTMODULE;
        }

        // Test unit
        if(match[19] == "Test unit"){
            testStructure->testUnit = match[20];
            testStructure->type = Type::TESTUNIT;
        }


        // Test group
        if(match[15] == "Test group"){
            testStructure->testGroup = match[16];
            if (testStructure->type == TM_TESTMODULE)
                testStructure->type = Type::TM_TESTGROUP;
            else
                testStructure->type = Type::TESTGROUP;

        }

        // Test case
        if(match[23] == "Test case"){
            testStructure->testCase = match[24];
            if (testStructure->type == TM_TESTMODULE)
                testStructure->type = Type::TM_TESTCASE;
            else
                testStructure->type = Type::TESTCASE;
        }
        // End of "order of entries is important" block


        // Action
        if(match[26] == "finished")
            testStructure->result = Action::END;
        else if (match[5] == "started")
            testStructure->result = Action::BEGIN;
        /// @warning not tested
        else if (match[5] == "aborted")
            testStructure->result = Action::ABORT;
        else
            testStructure->result = Action::UNDEFINED;


        return testStructure;
    }else{
        std::cerr << "Regex match fail in '" << __PRETTY_FUNCTION__
                  << "' for line " << std::endl
                  << "\t" << line << std::endl;
    }

    return nullptr;
}

void TestStructure::write(File &file, std::ostream &stream)
{

}


}
}
