/*
 * Copyright (C) 2021 Michal Borsuk.
 * Contact: mborsuk@mborsuk.de
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Michal Borsuk.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <Vector/ASC/platform.h>
#include <Vector/ASC/Event.h>
#include <Vector/ASC/EthernetSymbols.h>
#include <Vector/ASC/vector_asc_export.h>

namespace Vector {
namespace ASC {

struct VECTOR_ASC_EXPORT EthernetBusStat final : Event {
    EthernetBusStat(): Event(EventType::EthernetBusStat) {}

    /** @copydoc EthTime */
    EthTime time {0.0};

    /** @copydoc EthChannel */
    EthChannel channel {0};

    uint64_t HwRxPkts;
    uint64_t HwTxPkts;
    uint64_t HwRxError;
    uint64_t HwTxError;
    uint64_t HwRxBytes;
    uint64_t HwTxBytes;
    uint64_t HwRxNoBuffer;
    uint64_t HwSQIValue;

    /** @copydoc Event::read() */
    static EthernetBusStat * read(File & file, std::string & line);

    virtual void write(File & file, std::ostream & stream) override;

};


}
}
