/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iomanip>
#include <regex>
#include <sstream>

#include <Vector/ASC/CanCommon.h>
#include <Vector/ASC/CanSymbolsRegEx.h>
#include <Vector/ASC/SystemVariables.h>

namespace Vector {
namespace ASC {

SystemVariables * SystemVariables::read(File & file, std::string & line) {
    static const std::regex regex(REGEX_STOL REGEX_Timestamp
                REGEX_WS "SV:"
                REGEX_ws REGEX_svtype
                REGEX_WS "0"
                REGEX_WS "1" REGEX_WS "(.+?)"
                REGEX_ws "=" REGEX_WS "(\\[" REGEX_WS ")?" "(.+?)" "(" REGEX_WS "\\])?"
            REGEX_ENDL);
    std::smatch match;
    if (std::regex_match(line, match, regex)) {
        auto * systemVariables = new SystemVariables;
        systemVariables->time = stodC(match[1]);
        systemVariables->name = match[3];
        switch (std::stoul(match[2])) {
        case 1:
        {
            systemVariables->svType = SvType::Float;
            systemVariables->value.dblValue = std::stod(match[5]);
            break;
        }
        case 2:
        {
            systemVariables->svType = SvType::Int;
            systemVariables->value.intValue = std::stoul(match[5], nullptr, file.base);
            break;
        }
        case 3:
        {
            systemVariables->svType = SvType::String;
            systemVariables->value.strValue = new std::string(match[5]);
            break;
        }
        case 4:
        {
            /// @warning no data to test this
            systemVariables->svType = SvType::FloatArray;
            systemVariables->value.dblArray = new std::vector<double>;
            std::istringstream iss(match[5]);
            iss >> std::hex;
            while (!iss.eof()) {
                double s;
                iss >> s;
                systemVariables->value.dblArray->push_back(s);
            }
            break;
        }
        case 99:
        case 5:
        {
            systemVariables->svType = SvType::IntArray;
            systemVariables->value.intArray = new std::vector<uint32_t>;
            std::istringstream iss(match[5]);
            iss >> std::hex;
            while (!iss.eof()) {
                uint32_t s;
                iss >> s;
                systemVariables->value.intArray->push_back(s);
            }
            break;
        }
        case 7:
        {
            systemVariables->svType = SvType::ByteArray;
            systemVariables->value.byteArray = new std::vector<uint8_t>;
            std::istringstream iss(match[5]);
            iss >> std::hex;
            while (!iss.eof()) {
                uint16_t s; // this is not an error
                iss >> s;
                systemVariables->value.byteArray->push_back(s);
            }
            break;
        }
        default:
            std::clog << "Warning, default reached in " << __PRETTY_FUNCTION__
                      << std::endl;
        }
        return systemVariables;
    }else{
        std::cerr << "Regex match fail in '" << __PRETTY_FUNCTION__
                  << "' for line " << std::endl
                  << "\t" << line << std::endl;
    }


    return nullptr;
}

void SystemVariables::write(File & file, std::ostream & stream) {

    writeTime(file, stream, time);
    stream << "    ";

    /* format: "SV: %d %d %d %s = " */
    stream
            << "SV: " << (uint16_t) svType
            << ' ' << (flag[0] ? '1' : '0')
            << ' ' << (flag[1] ? '1' : '0')
            << ' ' << name
            << " = ";

    switch (svType) {
    case SvType::Float:
        stream << value.dblValue;
        break;
    case SvType::Int:
        stream << value.intValue;
        break;
    case SvType::String:
        stream << value.strValue;
        break;
    case SvType::FloatArray:     
    {
        /// @warning not tested
        for (auto element: *value.dblArray) {
            stream << ' ' << std::to_string(element);
        }
        break;
    }
    case SvType::IntArray:
    {
        /// @warning not tested
        for (auto element: *value.intArray) {
            stream << ' ' << std::to_string(element);
        }
        break;
    }
    case SvType::ByteArray:
    {
        /// @warning not tested
        for (auto element: *value.byteArray) {
            stream << ' ' << std::to_string(element);
        }
        break;
    }
    default:
        std::clog << "Warning, default reached in " << __PRETTY_FUNCTION__
                  << std::endl;
    }


    stream << endl;
}

}
}
