/*
 * Copyright (C) 2021 Michal Borsuk.
 * Contact: mborsuk@mborsuk.de
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Michal Borsuk.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <regex>

#include <Vector/ASC/EthernetBusStat.h>

#include <Vector/ASC/EthernetCommon.h>
#include <Vector/ASC/EthernetSymbolsRegEx.h>

Vector::ASC::EthernetBusStat *Vector::ASC::EthernetBusStat::read(File &/*file*/,
                                                                 std::string &line)
{
    static const std::regex regex(REGEX_STOL REGEX_Eth_Time
                REGEX_WS "ETH" REGEX_WS REGEX_Eth_Channel
                REGEX_WS "BUSSTATISTIC"
                REGEX_WS "HwRxPkts:" REGEX_ws "(.+?)"
                REGEX_WS "HwTxPkts:" REGEX_ws "(.+?)"
                REGEX_WS "HwRxError:" REGEX_ws "(.+?)"
                REGEX_WS "HwTxError:" REGEX_ws "(.+?)"
                REGEX_WS "HwRxBytes:" REGEX_ws "(.+?)"
                REGEX_WS "HwTxBytes:" REGEX_ws "(.+?)"
                REGEX_WS "HwRxNoBuffer:" REGEX_ws "(.+?)"
                REGEX_WS "HwSQIValue:" REGEX_ws "(.+?)"
            REGEX_ENDL);

    std::smatch match;
    if (std::regex_match(line, match, regex)) {
        auto * ethernetBusStat = new EthernetBusStat;

        ethernetBusStat->time = stodC(match[1]);
        ethernetBusStat->channel = std::stoul(match[2]);
        if (match[3] != "")
            ethernetBusStat->HwRxPkts = std::stoull(match[3]);
        if (match[4] != "")
            ethernetBusStat->HwTxPkts = std::stoull(match[4]);
        if (match[5] != "")
            ethernetBusStat->HwRxError = std::stoull(match[5]);
        if (match[6] != "")
            ethernetBusStat->HwTxError = std::stoull(match[6]);
        if (match[7] != "")
            ethernetBusStat->HwRxBytes = std::stoull(match[7]);
        if (match[8] != "")
            ethernetBusStat->HwTxBytes = std::stoull(match[8]);
        if (match[9] != "")
            ethernetBusStat->HwRxNoBuffer = std::stoull(match[9]);
        if (match[10] != "")
            ethernetBusStat->HwSQIValue = std::stoull(match[10]);

        return ethernetBusStat;
    }else{
        std::cerr << "Regex match fail in '" << __PRETTY_FUNCTION__
                  << "' for line " << std::endl
                  << "\t" << line << std::endl;
    }

    return nullptr;

}


void Vector::ASC::EthernetBusStat::write(File &/*file*/, std::ostream &/*stream*/)
{
    /// @warning not implemented
}
